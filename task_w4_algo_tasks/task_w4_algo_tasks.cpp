#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <array>

using namespace std;

string ascii_capitalizer(string c);

int main()
{
	vector<string> addEnding{"clever", "meek", "hurried", "nice"};
	auto add = [](string &s) {return s=s + "ly";};
	for_each(addEnding.begin(), addEnding.end(), add);	
	for(auto i : addEnding) { cout << i << " ";}
	cout << endl;

	array<int, 5> existsHigher{5, 3, 15, 22, 4};
	if(any_of(existsHigher.begin(), existsHigher.end(), [](int x){return x >= 50;}))
	{
		cout << "true" << endl;
	}
	else 
	{
		cout << "false" << endl;
	}

	string ascii = "to be or not to be";
	string asc2 = ascii_capitalizer(ascii);
	cout << asc2 << endl;

	return 0;
}

string ascii_capitalizer(string c)
{
	for_each(c.begin(), c.end(), [](char &c){if(c % 2 == 0) c = toupper(c); else c = tolower(c);});
	
	return c;
}