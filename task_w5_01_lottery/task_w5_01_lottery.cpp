#include <iostream>
#include <algorithm>
#include <set>
#include <chrono>
#include <random>

using namespace std;

int main()
{
	srand(time(nullptr));
	int licznik = 0;

	set<int> kule;
	
	for(int i = 0; i < 100; i++)
	{
		kule.insert(i);
	}

	
	while(!kule.empty())
	{
		kule.erase(rand()%100);
		licznik++;
		if(licznik % 100 == 0)
		{
			cout << "Przeprowadzono : " << licznik << " losowań." << endl;
			cout << "[";
			for(auto i : kule)
				cout << i << " ";
			cout << "]" << endl;
		}

	}
	cout << "Przeprowadzono : " << licznik << " losowań." << endl;
	return 0;
}