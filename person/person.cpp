#include <iostream>
#include <algorithm>
#include <string>
#include <array>
#include <chrono>
#include <random>


using namespace std;

class Person {
	string name;
	string surname;
	int age;
public:
	string get_name()
	{
		return name;
	}

	string get_surname()
	{
		return surname;
	}
	int get_age()
	{
		return age;
	}

	Person(string n, string s, int a)
	{
		name =n;
		surname = s;
		age = a;
	}

	Person() = default;
	friend std::ostream& operator<<(std::ostream& s,const Person & p)
	{
		s << p.name << ' ' << p.surname << ' ' << p.age;
		return s;
	}
};

bool po_imieniu(Person & lewy, Person & prawy)
{
	return lewy.get_name() < prawy.get_name();
}

bool po_nazwisku(Person & lewy, Person & prawy)
{
	return lewy.get_surname() < prawy.get_surname();
}

bool po_wieku(Person & lewy, Person & prawy)
{
	return lewy.get_age() < prawy.get_age();
}

int main()
{
	cout << "1) Zaimplementowanie operatora 'operator<<' dla klasy Person." << endl;
	Person p("Zdzislaw", "Nowak", 56);
	cout << p << endl;

	cout << endl;
	cout << "2) Zaimplementowanie funkcji do generowania losowej osoby." << endl;
	string names[] {"Zdzislaw", "Damian", "Tomek", "Przemek", "Szymon", "Magda", "Dominika", "Ala", "Monika", "Kasia"};
	string surnames[] {"Kowalczyk", "Nowak", "Wojcik", "Majchrzak", "Czajka", "Domagała", "Mazur", "Stefaniak", "Janik", "Krawczyk"};
	
	srand(time(nullptr));

	for(int i = 0; i < 10; i++)
	{
		
		Person g(names[rand() % 10], surnames[rand() % 10], rand() % 90 + 10);
		cout << g << endl;
	}

	cout << endl;
	cout << "3) Wygenerować 10 losowych osób i umieścić w kontenerze typu std::array<>" << endl;
	array<Person, 10> gen; 
	for(int i = 0; i < 10; i++)
	{
		
		gen[i] = Person(names[rand() % 10], surnames[rand() % 10], rand() % 90 + 10);
		cout << gen[i] << endl;
	}

	cout << endl;
	cout << "Posortowane osoby rosnąco względem imienia:" << endl;
	sort(gen.begin(), gen.end(), po_imieniu);
	for(int i = 0; i < 10; i++)
	{
		cout << gen[i] << endl;
	}

	cout << endl;
	cout << "Posortowane osoby rosnąco względem nazwiska:" << endl;
	sort(gen.begin(), gen.end(), po_nazwisku);
	for(int i = 0; i < 10; i++)
	{
		cout << gen[i] << endl;
	}

	cout << endl;
	cout << "Posortowane osoby rosnąco względem wieku:" << endl;
	sort(gen.begin(), gen.end(), po_wieku);
	for(int i = 0; i < 10; i++)
	{
		cout << gen[i] << endl;
	}

	cout << endl;
	cout << "Posortowane osoby malejąco względem imienia: " << endl;
	sort(gen.rbegin(), gen.rend(), po_imieniu);
	for(int i = 0; i < 10; i++)
	{
		cout << gen[i] << endl;
	}

	cout << endl;
	cout << "Posortowane osoby malejąco względem nazwiska:" << endl;
	sort(gen.rbegin(), gen.rend(), po_nazwisku);
	for(int i = 0; i < 10; i++)
	{
		cout << gen[i] << endl;
	}

	cout << endl;
	cout << "Posortowane osoby malejąco względem wieku:" << endl;
	sort(gen.rbegin(), gen.rend(), po_wieku);
	for(int i = 0; i < 10; i++)
	{
		cout << gen[i] << endl;
	}

	return 0;
}