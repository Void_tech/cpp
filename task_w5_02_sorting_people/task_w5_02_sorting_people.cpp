#include <iostream>
#include <algorithm>
#include <set>
#include <string>
#include <chrono>
#include <random>


using namespace std;

class Person {
	string name;
	string surname;
	int age;
   public:
        Person() = default;
        Person(string n, string s, int a)
	{
		name =n;
		surname = s;
		age = a;
	}
	string get_name()
	{
		return name;
	}

	string get_surname()
	{
		return surname;
	}
	int get_age()
	{
		return age;
	}


	bool operator<(const Person & p1) const
	{
		string pl = p1.surname + p1.name + to_string(p1.age);
		string pr =  this->surname + this->name + to_string(this->age);	
		return (pl > pr); 
		
	}
	friend std::ostream& operator<<(std::ostream& s,const Person & p)
	{
		s << p.name << ' ' << p.surname << ' ' << p.age;
		return s;
	}
};


int main()
{
	string names[] {"Zdzislaw", "Damian", "Tomek", "Przemek", "Szymon", "Magda", "Dominika", "Ala", "Monika", "Kasia"};
	string surnames[] {"Kowalczyk", "Nowak", "Wojcik", "Majchrzak", "Czajka", "Domagała", "Mazur", "Stefaniak", "Janik", "Krawczyk"};

	set<Person> ludzie;
	srand(time(nullptr));

	for(int i = 0; i < 1000; i++)
	{
		Person p(names[rand() % 10], surnames[rand() % 10], rand() % 90 + 10);
		ludzie.insert(p);
	}
	
	for(set<Person>::iterator it=ludzie.begin(); it!=ludzie.end(); ++it)
	{
		cout << *it << endl;
	}
	
	return 0;
}