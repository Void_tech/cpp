#include <iostream>
#include "wczytaj.h"
#include "dodaj.h"

using namespace std;

int x, y;

int main()
{
	cout << "Podaj wartosc x: ";
	x = wczytaj();
	cout << "Podaj wartosc y: ";
	y = wczytaj();

	cout << "Suma liczb " << x << " i " << y << " rowna sie: " << dodaj(x, y) << endl;

	return 0;
}

